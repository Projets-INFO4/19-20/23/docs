This document is obsolete. Please refer to this [one](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/23/docs/-/blob/master/minimum_setup.md) instead.

# `oardocker` installation / setup

Make sure docker is installed before launching these commands.

- `sudo su`
- `python3 -m venv env` (or `python`, `sudo apt install python3-venv
` might be needed before)
- `source env/bin/activate`
- `pip3 install git+https://github.com/oar-team/oar-docker.git` (or `pip`)
- `oardocker init`
- `oardocker build`
- `oardocker install git+https://github.com/oar-team/oar.git`
- `oardocker start -n 3`

Try to access the API : <http://localhost:46668/oarapi/timezone.json>

# `oardocker` launch

- `source env/bin/activate`
- `sudo oardocker start -n 3`

## Installing docker on `Ubuntu Eoan Ermine 19.10`

Download `.deb` packages from [here](https://download.docker.com/linux/ubuntu/dists/eoan/).

- `sudo dpkg -i containerd.io_1.2.11-2_amd64.deb`
- `sudo dpkg -i docker-ce_19.03.6_2.1.rc1-0_ubuntu-eoan_amd64.deb`
- `sudo dpkg -i docker-ce-cli_19.03.6_2.1.rc1-0_ubuntu-eoan_amd64.deb`
