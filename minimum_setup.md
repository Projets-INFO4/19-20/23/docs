The easiest way to get a dashboard running is using oardocker.

# `oardocker` installation

Here is the list of command we used to install and launch oardocker.
First, make sure docker is [installed ](https://docs.docker.com/engine/install/).

- `pip3 install git+https://github.com/oar-team/oar-docker.git`
- `sudo oardocker init`
- `sudo oardocker build`
- `git clone https://github.com/oar-team/oar3.git`
- `sudo oardocker install ./oar3`

Start a cluster :

- `sudo oardocker start -n 3`

# `oar-dashboard`

Here is the list of command we used to install and launch `oar-dashboard`.

- `git clone git@gricad-gitlab.univ-grenoble-alpes.fr:Projets-INFO4/19-20/23/oar-dashboard.git`
- `yarn install`

Start the dashboard : 

- `yarn start`
