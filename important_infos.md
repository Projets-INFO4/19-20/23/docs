# Useful links

- [`react-admin` doc](https://marmelab.com/react-admin/Readme.html)
- [Material UI website](https://material-ui.com/)
- [Angular code of last year]()
- [Wiki air of the project](https://air.imag.fr/index.php/Portail_pour_gestionnaire_de_taches)
- [Gitlab of the project](https://gricad-gitlab.univ-grenoble-alpes.fr/Projet-RICM4/17-18/2/Dashboard)
- [README.md](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/23)
- [Oar API doc](http://oar.imag.fr/docs/latest/user/api.html)
- [`oar-docker` github](https://github.com/oar-team/oar-docker)
- [Proxying API requests in react](https://create-react-app.dev/docs/proxying-api-requests-in-development/)

# Setting up a `React-Admin` typescript project

<https://marmelab.com/react-admin/Tutorial.html>

Create a simple `react-admin` app :

- `sudo npm install -g yarn`
- `yarn create react-app test-admin --template typescript`
- `cd test-admin/`
- `yarn add react-admin ra-data-json-server prop-types`
- `yarn start`

## Making Contact With The API Using a Data Provider

A `React-Admin` application need an `<Admin>` component. This component need a `dataProvider` able to fetch data from an API. We will need to write our own `dataProvider` in the future to connect `React-Admin` to the oar API. For know we are using a `dataProvider` compatible with the fake `JSONPlaceholder` API given by the `ra-data-json-server` module.


## Multiple data provider (possible future issue)

<https://stackoverflow.com/questions/52889909/how-is-the-correct-way-to-have-multiple-dataproviders-in-react-admin>

Basically create two data provider and assign ressources to one or the other.
Useless so far, one dataProvider seems good enough.

# Typescript introduction

<https://www.youtube.com/watch?v=ahCwqrYpIuM>
